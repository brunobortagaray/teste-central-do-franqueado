const express = require('express');
const router = express.Router();
const Util = require('../models/Util');

/* GET home page. */
router.get('/', function(req, res) {

    const numElementos = 100;
    const numPorColuna = 25;

    // Separa os numeros em colunas na pagina inicial: 100 numeros no total, 25 por coluna, neste caso
    const colunas = Util.algoritmoCentralFranqueado(numElementos, numPorColuna);
    console.log("Renderizando a pagina: /");
    res.render('index', {
        title: 'Teste Central do Franqueado' ,
        subtitle: 'Desenvolver um algoritmo que print os numeros de 1 a 100, mas, substituindo ' +
        'multiplos de 3 por "Central", multiplos de 5 "Franqueado" e "CentralFranqueado" para multiplos tanto de 3 e 5 .',
        colunas: colunas,
    });
});

module.exports = router;
