/**
 * Classe com métodos uteis para o projeto
 */
class Util {

    /**
     * algoritmoCentralFranqueado
     * Verifica se os numeros são multiplos de 3 ou 5:
     * Se multiplos de 3, printa "Central" ao invés do numero
     * Se multiplos de 5, printa "Franqueado" ao invés do numero
     * Se multiplo de ambos, printa "CentralFranqueado"
     * Caso não seja multiplo de nenhum deles, printa o numero na tela
     *
     * @param numTermos int Define quantos termos serão percorridos
     * @param numTermosColuna int Define quantos termos terao por cada coluna
     */
    static algoritmoCentralFranqueado(numTermos, numTermosColuna) {
        console.log("Executando: algoritmoCentralFranqueado(" + numTermos + ", " + numTermosColuna + ")");
        let resultado = [];
        let valores = [];

        for(let i = 1; i <= numTermos; i++) {
            let numeroAtual = i;
            // Caso seja multiplo de 3 e 5 ao mesmo tempo
            if(i % 3 === 0 && i % 5 === 0) {
                numeroAtual = "CentralFranqueado";
            } else if(i % 3 === 0){ // Multiplo de 3
                numeroAtual = "Central";
            } else if(i % 5 === 0){ // Multiplo de 5
                numeroAtual = "Franqueado";
            } else {
                numeroAtual = i;
            }
            //Cria as colunas a serem inseridas
            valores.push(numeroAtual);

            // Quando completa a quantidade de termos por coluna, adiciona-se outra coluna
            if(i % numTermosColuna === 0) {
                let objColuna = {
                    titulo: Util.prototype.tituloColunaAlgoritmo(numTermosColuna, i),
                    valores: valores,
                };

                // Cria um array de objetos para a view
                resultado.push(objColuna);
                valores = [];
            }
        }

        return resultado;
    }

    /**
     * tituloColunaAlgoritmo
     * Cria o titulo da coluna na listagem de colunas no formato "1º Elemento-Ultimo Elemento.
     * Ex: 1-25, 26-50...
     *
     * @param numTermosColuna int Define quantos termos terao em cada coluna
     * @param ultimoElemento int Define o ultimo elemento do titulo
     */
    tituloColunaAlgoritmo(numTermosColuna, ultimoElemento) {
        console.log("Executando: tituloColunasAlgoritmo(" + numTermosColuna + ", " + ultimoElemento + ")");

        //Formula da a criação do título para que fique no formato: "primeiro_elemento-ultimo_elemento"
        let tituloString = 1 + ultimoElemento - numTermosColuna + "-" + ultimoElemento;
        return tituloString;
    }
}

module.exports = Util;