const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

// Arquivo onde pegará as rotas iniciais (No caso, cairá diretamente no algoritmo)
const index = require('./routes/index');

const app = express();

// Sera a engine da view (Handlebars)
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// Favicon
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Utiliza a rota específica
app.use('/', index);

// Pega os erros 404 e encaminha para o handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Handler de erro
app.use(function(err, req, res, next) {
  // Define as constiaveis locais. So exibe o erro no ambiente de dev
  res.locals.message = err.message;
    res.locals.error = req.app.get('env') !== 'development' ? {} : err;

  // Renderiza a pagina de erro
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
