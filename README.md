# Teste Central do Franqueado #

Este é o repositório do meu teste para a empresa Central do Franqueado.

### Como faço para rodar o projeto? ###

* Clone o repositorio
* Na linha de comando, rode o comando `npm run start`
* O site estará disponivel no [localhost](localhost:3000)

### Por que nao ignorar o node_modules? ###

Optei por não ignorar o node_modules para ter certeza que o projeto
já viria com todas as dependencias instaladas. Nunca se sabe. Vai que o NPM da pau
amanha e o projeto fica sem as dependencias... =)

### Quais os principais modulos do projeto? ###

* Express
* Babel (babel-cli) para que nao haja problema de compatibilidade por estar utilizando o ES6
* Nodemon em dev, para reiniciar o servidor a cada modificação
* Morgan como tracker de routes
* Handlebars para views